# Template to build maven projects.

## Usage

Example file `.gitlab-ci.yml` can look like this:

```yaml
include:
  - project: ryszardszewczyk/web/backend/java/build-tools/ci-templates
    file: java/maven/v1.yml

variables:
  # maven template configuration
  JAVA_CI_VERSION: 11
  JAVA_CI_LINT_QODANA: 1
  JAVA_CI_LINT_SONAR: 1
  JAVA_CI_FORMAT: 1
```

## Global configuration

Template is written for projects, which use generic pom, which
is stored and described in a separate repository - add link to it.
In said repository are stored auxiliary files as well, including Makefile
for running Qodana locally, and description on how to integrate some of the
tools (mostly linters) with code editor/IDE.

Jobs can be configured with a help of global variables, just as is
demonstrated below:

| global variable          | description                                                                              |
| ------------------------ | ---------------------------------------------------------------------------------------- |
| `JAVA_CI_VERSION`        | JDK version to use.                                                                      |
| `JAVA_CI_PROFILES`       | List of maven profiles that are active during execution.                                 |
| `JAVA_CI_ALWAYS_RUN`     | Enforce step execution.                                                                  |
| `JAVA_CI_CLEAR_CACHE`    | Enforce clearing cache before job execution.                                             |
| `JAVA_CI_LATEST_LTS_JDK` | Newest JDK version (used by tools that require the most up-to-date JDK for them to work. |
| `MAVEN_CLI_OPTS`         | Additional options passed along side while calling `mvn`                                 |

In a case of variables that expect logical value, `1` means true,
and `0` false. At the end of job's note link to job documentation
is print that points to this file.

## Jobs

### test

|                    |                                                                   |
| ------------------ | ----------------------------------------------------------------- |
| Description        | triggers tests                                                    |
| Execution stage    | review                                                            |
| Local execution    | `mvn -P ci test`                                                  |
| CI faze            | `test`                                                            |
| GitLab integration | [code coverage][coverage], [executed test report][testing-report] |

### Configuration

| Environmental variable | Description                          |
| ---------------------- | ------------------------------------ |
| `JAVA_CI_TEST`         | Should tests be run?                 |
| `JAVA_CI_DIFF_COV`     | Should check for 100% code coverage. |

Rules allows for enforcing coverage of given classes, for example: coverage of all methods in a controller.

### style

|                    |                                                                                                                                     |
| ------------------ | ----------------------------------------------------------------------------------------------------------------------------------- |
| Description        | Checks code format with the help of Google Java Format                                                                              |
| Execution stage    | Review                                                                                                                              |
| Local execution    | `java -jar <JAR GJF> --replace --skip-reflowing-long-strings $(find -name '*.java' -not -path '**/target/*' -not -path './.mvn/*')` |
| CI faze            | `test`                                                                                                                              |
| GitLab integration | Artefact's patch, when code is not formatted properly.                                                                              |

### lint

|                    |                                |
| ------------------ | ------------------------------ |
| Description        | Runs basic linters             |
| Execution stage    | Review                         |
| Local execution    | `mvn -P ci -DskipTests verify` |
| CI faze            | `test`                         |
| GitLab integration | [Lint report][lint-report]     |

If SonarScanner is used, `-Dsonar.projectKey=<project_key>` should be passed as well.

In CI linting is not aborted, when problems in a module are detected.
Whole project is always linted and only at the end all the problem are
reported back to a user. If any problems were detected, they will be
printed out at the end of the job's note in a shortened form.

### lint_qodana

|                    |                                                          |
| ------------------ | -------------------------------------------------------- |
| Description        | Runs Qodana/Intellij linting                             |
| Execution stage    | Review                                                   |
| Local execution    | `make -f <ścieżka/do/java/shared/Maven.Makefile> qodana` |
| CI faze            | `test`                                                   |
| GitLab integration | [Lint report][lint-report], SARIF report in an artefact  |

Report is published, and can be downloaded from job's page `lint_qodana` or
from the main page of a review.

Downloaded report can be opened in Intellij after plugin `Qodana` installation
that can be found in `Tools -> Qodana -> Open Qodana Analysis Report` menu.

There is also a possibility to run linters locally directly in Intellij
in `Code -> Inspect Code` menu.

Linters' configuration (profile) is shared between qodana and Intellij
and can be found in `.idea` directory. Profile can be configured
in [settings](https://www.jetbrains.com/help/idea/code-inspection.html)
(menu `File -> Settings -> Editor -> Inspections`).

### Configuration

| Environmental variable        | Description                                            |
| ----------------------------- | ------------------------------------------------------ |
| `JAVA_CI_LINT_QODANA`         | Should run Qodana linter?                              |
| `JAVA_CI_LINT_QODANA_PROFILE` | Linter's configuration profile to run.                 |
| `JAVA_CI_LINT_QODANA_BUILD`   | Should build project before linting? Default `1` (yes) |

Template contains default file with
[Qodana configuration](https://www.jetbrains.com/help/qodana/qodana-yaml.html#Set+up+a+profile)
If is not sufficient, file `qodana.yaml` should be added to a repository's directory with
your own rules. Built-in configuration allows for profile to be changed through variable
`JAVA_CI_LINT_QODANA_PROFILE.`
[Official configuration documentation](https://www.jetbrains.com/help/idea/customizing-profiles.html)
describes how to add your own rules from IDE.

Commonly used name for a profile is `rszewczyk`. Project profile's files should be
then added to a repository:

```shell
git add -f .idea/inspectionProfiles/Project_Default.xml
git add -f .idea/inspectionProfiles/rszewczyk.xml
git add -f .idea/inspectionProfiles/profiles_settings.xml
git add -f .idea/misc.xml
```

### owasp-dependency-check

Checks if project's dependencies contain known vulnerabilities.
[dependency-check](https://github.com/jeremylong/DependencyCheck)
is used for that.

|                    |                                       |
| ------------------ | ------------------------------------- |
| Description        | Checks if dependencies are vulnerable |
| Execution stage    | Review                                |
| Local execution    | `mvn -P owasp -DskipTests`            |
| CI faze            | `test`                                |
| GitLab integration | N/A. Report publication is available  |

### Configuration

| Environmental variable           | Description                    |
| -------------------------------- | ------------------------------ |
| `JAVA_CI_OWASP_DEPENDENCY_CHECK` | Should run dependencies check? |

[java-shared]: https://todo.com
[lint-report]: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html
[coverage]: https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html
[report-tests]: https://docs.gitlab.com/ee/ci/unit_test_reports.html
